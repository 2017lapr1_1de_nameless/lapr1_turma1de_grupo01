package lapr1_turma1de_grupo01;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.decomposition.SingularValueDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

public class LAPR1_Turma1DE_Grupo01 {

    public static Scanner ler = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fichImagem = nomeFicheiro(args);
        if (fichImagem == null) {
            System.out.println("O par�metro nao existe.");
        } else {
            int[][] imagem = ImportarImagem(fichImagem);
            int dim = imagem.length;
            int[][] aumentada = new int[dim + 2][dim + 2];
            System.out.println("Imagem:");
            mostrarImagem(imagem, dim);
            int op;
            do {
                menu();
                op = ler.nextInt();
                switch (op) {
                    case 1:
                        int opRodar;
                        do {
                            menuRodar();
                            opRodar = ler.nextInt();
                            switch (opRodar) {
                                case 1:
                                    imagem = rodarMatriz(imagem, dim);
                                    guardar(imagem, dim);
                                    break;
                                case 2:
                                    imagem = rodarMatriz90NaoHorario(imagem, dim);
                                    guardar(imagem, dim);
                                    break;
                                case 0:
                                    break;
                                default:
                                    System.out.println("Opcao invalida");
                                    break;
                            }
                        } while (opRodar != 0);
                        break;
                    case 2:
                        int opfiltros;
                        do {
                            menuFiltros();
                            opfiltros = ler.nextInt();
                            switch (opfiltros) {
                                case 1:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroMedia(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 2:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroMediana(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 3:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroMaximo(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 4:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroMinimo(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 5:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroVariancia(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 6:
                                    aumentar(dim, aumentada, imagem);
                                    imagem = filtroConvolucao(dim, imagem, aumentada);
                                    guardar(imagem, dim);
                                    break;
                                case 0:
                                    break;
                                default:
                                    System.out.println("Opcao invalida");
                                    break;
                            }
                        } while (opfiltros != 0);
                        break;
                    case 3:
                        caracteristicas(imagem, dim);
                        break;
                    case 4:
                        ordenarColunas(dim, imagem);
                        guardar(imagem, dim);
                        break;
                    case 5:
                        decompor(imagem, dim);
                        break;
                    case 6:
                        visualizar();
                        break;
                    case 0:
                        System.out.println("Fim");
                        break;
                    default:
                        System.out.println("Opcao Invalida");
                        break;
                }
            } while (op != 0);
        }
    }

    public static void menu() {
        System.out.println("Introduza:");
        System.out.println("1- Rodar a imagem");
        System.out.println("2- Filtrar a imagem");
        System.out.println("3- Caracteristicas da matriz");
        System.out.println("4- Ordenar cada uma das colunas da matriz");
        System.out.println("5- Comprimir Imagem");
        System.out.println("6- Visualizar Imagem");
        System.out.println("0- Fim");
    }

    public static void menuRodar() {
        System.out.println("Introduza:");
        System.out.println("1- Sentido horario");
        System.out.println("2- Sentido anti horario");
        System.out.println("0- Fim do Rodar");
    }

    public static String nomeFicheiro(String[] args) {
        String fichImagem;
        if (args.length < 1 || args.length > 1) {
            System.out.println("Numero de parametros introduzidos e diferente de 1");
            System.out.println("Introduza o nome do ficheiro: ");
            fichImagem = ler.nextLine();
        } else {
            fichImagem = args[0];
        }
        boolean exists = (new File(fichImagem).exists());
        if (exists == true) {
            return fichImagem;
        } else {
            return null;
        }
    }

    public static String nomeFicheiro() {
        String fichImagem;
        System.out.println("Introduza o nome do ficheiro");
        fichImagem = ler.next();
        return fichImagem;
    }

    public static int[][] ImportarImagem(String fichImagem) throws FileNotFoundException {

        Scanner in = new Scanner(new File("Imagens/" + fichImagem));
        String linha = in.nextLine();
        String[] tmp = linha.split("  ");
        int dim = Integer.parseInt(tmp[1]);
        int[][] imagem = new int[dim][dim];
        in.nextLine();
        int j = 0;
        while (j < dim) {
            linha = in.nextLine();
            String[] temp = linha.split(",");
            for (int i = 0; i < dim; i++) {
                imagem[j][i] = Integer.parseInt(temp[i]);
            }
            j++;
        }
        in.close();
        return imagem;
    }

    public static void menuFiltros() {
        System.out.println("Introduza:");
        System.out.println("1- Filtro media");
        System.out.println("2- Filtro mediana");
        System.out.println("3- Filtro maximo");
        System.out.println("4- Filtro minimo");
        System.out.println("5- Filtro variancia");
        System.out.println("6- Filtro de Convolucao");
        System.out.println("0- Voltar ao menu");
    }

    public static void visualizar() throws IOException {
        String nomeFicheiro = null, diretorio;
        File dir;
        do {
            System.out.println("Introduza o nome do diretorio");
            diretorio = ler.next();
            dir = new File(diretorio + "/.");
        } while (!(dir.exists()));
        do {
        File[] filesList = dir.listFiles();
        for (File file : filesList) {
            if (file.isFile()) {
                System.out.println(file.getName());
            }
        }
            System.out.println("--Introduza o nome do ficheiro que deseja ler--");
            System.out.println("--Para terminar introduza 'ACABAR'--");
            nomeFicheiro = ler.next();
            if (!nomeFicheiro.equals("ACABAR")) {
                Scanner in = new Scanner(new File("Imagens/" + nomeFicheiro));
                String linha = in.nextLine();
                if (linha.contains(".")) {
                    int [][]imagemReconstruida=reconstruirImagem(nomeFicheiro);
                    int tamanho= imagemReconstruida.length;
                    guardar(imagemReconstruida,tamanho);
                    
                } else {
                    Runtime rt = Runtime.getRuntime();
                    Process prcs = rt.exec("cmd.exe /c start gnuplot  -e filename='Imagens/" + nomeFicheiro + "' exemploGnuplot.gp");
                }
            }
        } while (!nomeFicheiro.equals("ACABAR"));
    }

    public static int[][] rodarMatriz(int[][] imagem, int dim) {
        int[][] rodada = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                rodada[i][j] = imagem[dim - j - 1][i];
            }
            System.out.println(Arrays.toString(rodada[i]));
        }
        System.out.println("Imagem rodada sentido horario:");
        return rodada;
    }

    public static void guardar(int[][] imagem, int dim) throws FileNotFoundException {
        System.out.println("Deseja guardar o ficheiro? (sim ou nao)");
        String opcao = ler.next();
        while (!opcao.equals("sim") && !opcao.equals("nao")) {
            System.out.println("Opcao invalida introduza de novo");
            System.out.println("Deseja guardar o ficheiro? (sim ou nao)");
            opcao = ler.next();
        }
        if (opcao.equals("sim")) {
            System.out.println("Introduza o nome do ficheiro em que pretende guardar a imagem");
            String ficheiro = ler.next();
            Formatter output = new Formatter(new File("Imagens/" + ficheiro));
            Formatter output2 = new Formatter(new File(ficheiro));
            output.format(ficheiro + "  " + dim + "\n");
            output2.format(ficheiro + "  " + dim + "\n");
            for (int i = 0; i < dim; i++) {
                String linha = Arrays.toString(imagem[i]);
                linha = linha.replace("[", "");
                linha = linha.replace("]", "");
                linha = linha.replaceAll(" ", "");
                output.format("\n" + linha);
                output2.format("\n" + linha);
            }
            output.close();
            output2.close();
        }
    }

    public static void aumentar(int dim, int[][] aumentada, int[][] imagem) {
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                aumentada[i + 1][j + 1] = imagem[i][j];
            }
        }
        for (int k = 0; k < dim + 1; k++) {
            aumentada[0][k + 1] = aumentada[1][k + 1];
            aumentada[k + 1][0] = aumentada[k + 1][1];
            aumentada[dim + 1][k] = aumentada[dim][k];
            aumentada[k][dim + 1] = aumentada[k][dim];
        }
        aumentada[0][0] = 0;
        aumentada[0][dim + 1] = 0;
        aumentada[dim + 1][0] = 0;
        aumentada[dim + 1][dim + 1] = 0;
    }

    public static int[][] filtroMedia(int dim, int[][] imagem, int[][] aumentada) {
        int soma, tamanho = 9;
        int[][] filtrada = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                soma = 0;
                for (int k = 0; k < 3; k++) {
                    for (int w = 0; w < 3; w++) {
                        soma = soma + (aumentada[i + k][j + w]);
                    }
                }
                filtrada[i][j] = (soma / tamanho);
            }
        }
        mostrarImagem(filtrada, dim);
        System.out.println("Imagem filtrada:");
        return filtrada;
    }

    public static int[][] filtroMediana(int dim, int[][] imagem, int[][] aumentada) {
        int tamanho = 9, count = 0;
        int[][] filtrada = new int[dim][dim];
        int[] vetor = new int[tamanho];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                for (int w = 0; w < 3; w++) {
                    for (int v = 0; v < 3; v++) {
                        vetor[count] = aumentada[i + w][j + v];
                        count++;
                    }
                }
                ordenarVetor(vetor, tamanho);
                filtrada[i][j] = vetor[4];
                count = 0;
            }
        }
        mostrarImagem(filtrada, dim);
        System.out.println("Imagem filtrada:");
        return filtrada;
    }

    public static void ordenarVetor(int[] vetor, int tamanho) {
        int aux;
        for (int i = 0; i < tamanho - 1; i++) {
            for (int j = i + 1; j < tamanho; j++) {
                if (vetor[i] < vetor[j]) {
                    aux = vetor[i];
                    vetor[i] = vetor[j];
                    vetor[j] = aux;
                }
            }
        }
    }

    public static int[][] filtroMaximo(int dim, int[][] imagem, int[][] aumentada) {
        int maior, tamanho = 9;
        int[][] filtrada = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                maior = 0;
                for (int k = 0; k < 3; k++) {
                    for (int w = 0; w < 3; w++) {
                        if (aumentada[i + k][j + w] > maior) {
                            maior = aumentada[i + k][j + w];
                        }
                    }
                }
                filtrada[i][j] = maior;
            }
        }
        mostrarImagem(filtrada, dim);
        System.out.println("Imagem filtrada:");
        return filtrada;
    }

    public static void mostrarImagem(int[][] imagem, int dim) {
        for (int v = 0; v < dim; v++) {
            System.out.println(Arrays.toString(imagem[v]));
        }
    }

    public static int[][] filtroMinimo(int dim, int[][] imagem, int[][] aumentada) {
        int menor, tamanho = 9;
        int[][] filtrada = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                menor = 255;
                for (int k = 0; k < 3; k++) {
                    for (int w = 0; w < 3; w++) {
                        if (aumentada[i + k][j + w] < menor) {
                            menor = aumentada[i + k][j + w];
                        }
                    }
                }
                filtrada[i][j] = menor;
            }
        }
        mostrarImagem(filtrada, dim);
        System.out.println("Imagem filtrada:");
        return filtrada;
    }

    public static void caracteristicas(int[][] imagem, int dim) {
        int opcao;
        do {
            System.out.println("1-Soma de uma das linhas");
            System.out.println("2-Soma de uma das colunas");
            System.out.println("3-Media de uma das linhas");
            System.out.println("4-Media de uma das colunas");
            System.out.println("5-Soma total");
            System.out.println("6-Media total");
            System.out.println("7-Determinar os valores e vetor proprios");
            System.out.println("8-Regra do trapezio");
            System.out.println("0-Terminar");
            opcao = ler.nextInt();
            switch (opcao) {
                case 1:
                    somaLinha(imagem, dim);
                    break;
                case 2:
                    somaColuna(imagem, dim);
                    break;
                case 3:
                    mediaLinha(imagem, dim);
                    break;
                case 4:
                    mediaColuna(imagem, dim);
                    break;
                case 5:
                    somaTotal(imagem, dim);
                    break;
                case 6:
                    mediaTotal(imagem, dim);
                    break;
                case 7:
                    vetorProprio(imagem, dim);
                    break;
                case 8:
                    integracao(imagem, dim);
                    break;
                default:
                    System.out.println("Opcao Invalida");
                    break;
                case 0:
                    System.out.println("Voltar ao menu");
                    break;
            }
        } while (opcao != 0);
    }

    public static void somaLinha(int[][] imagem, int dim) {
        int linha;
        do {
            System.out.println("Introduza o indice da linha para a qual pretende visualizar a sua media(entre 1 e " + dim + ")");
            linha = ler.nextInt();
            if (linha < 1 || linha > dim) {
                System.out.println("Opcao invalida");
            }
        } while (linha < 1 || linha > dim);
        int soma = 0;
        for (int i = 0; i < dim; i++) {
            soma = soma + imagem[linha - 1][i];
        }
        System.out.println("Soma da linha " + linha + ": " + soma);
    }

    public static void somaColuna(int[][] imagem, int dim) {
        int coluna;
        do {
            System.out.println("Introduza o indice da coluna para a qual pretende visualizar a sua media(entre 1 e " + dim + ")");
            coluna = ler.nextInt();
            if (coluna < 1 || coluna > dim) {
                System.out.println("Opcao invalida");
            }
        } while (coluna < 1 || coluna > dim);
        int soma = 0;
        for (int i = 0; i < dim; i++) {
            soma = soma + imagem[i][coluna - 1];
        }
        System.out.println("Soma da coluna " + coluna + ": " + soma);
    }

    public static void mediaLinha(int[][] imagem, int dim) {
        int linha;
        do {
            System.out.println("Introduza o indice da linha para a qual pretende visualizar a sua media(entre 1 e " + dim + ")");
            linha = ler.nextInt();
            if (linha < 1 || linha > dim) {
                System.out.println("Opcao invalida");
            }
        } while (linha < 1 || linha > dim);
        int soma = 0;
        for (int i = 0; i < dim; i++) {
            soma = soma + imagem[linha - 1][i];
        }
        int media = soma / dim;
        System.out.println("Media da linha " + linha + ": " + media);
    }

    public static void mediaColuna(int[][] imagem, int dim) {
        int coluna;
        do {
            System.out.println("Introduza o indice da coluna para a qual pretende visualizar a sua media(entre 1 e " + dim + ")");
            coluna = ler.nextInt();
            if (coluna < 1 || coluna > dim) {
                System.out.println("Opcao invalida");
            }
        } while (coluna < 1 || coluna > dim);
        int soma = 0;
        for (int i = 0; i < dim; i++) {
            soma = soma + imagem[i][coluna - 1];
        }
        int media = soma / dim;
        System.out.println("Media da coluna " + coluna + ": " + media);
    }

    public static int somaTotal(int[][] imagem, int dim) {
        int soma = 0;
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                soma = soma + imagem[i][j];
            }
        }
        System.out.println("Soma total da matriz: " + soma);
        return soma;
    }

    public static int mediaTotal(int[][] imagem, int dim) {
        int soma = 0, media;
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                soma = soma + imagem[i][j];
            }
        }
        media = soma / (dim * dim);
        System.out.println("Media da matriz: " + media);
        return media;
    }

    public static void vetorProprio(int[][] imagem, int dim) {
        double[][] imagemDouble = new double[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                imagemDouble[i][j] = imagem[i][j];
            }
        }
        Matrix imagemMatrix = new Basic2DMatrix(imagemDouble);
        EigenDecompositor eigenD = new EigenDecompositor(imagemMatrix);
        Matrix[] mattD = eigenD.decompose();
        for (int i = 0; i < 2; i++) {
            System.out.println(mattD[i]);
        }
    }

    public static void ordenarColunas(int dim, int[][] imagem) {
        int[] coluna = new int[dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                coluna[j] = imagem[j][i];
            }
            insertionSort(coluna);
            for (int k = 0; k < dim; k++) {
                imagem[k][i] = coluna[k];
            }
        }
        mostrarImagem(imagem, dim);
    }

    public static void insertionSort(int vetor[]) {
        int n = vetor.length;
        for (int i = 1; i < n; ++i) {
            int aux = vetor[i];
            int j = i - 1;
            while (j >= 0 && vetor[j] > aux) {
                vetor[j + 1] = vetor[j];
                j = j - 1;
            }
            vetor[j + 1] = aux;
        }
    }

    public static int[][] filtroVariancia(int dim, int[][] imagem, int[][] aumentada) {
        int soma, media, TAMANHO = 9, DIMFILTRO = 3, somaDesv;
        int[][] filtrada = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                soma = 0;
                somaDesv = 0;
                for (int k = 0; k < DIMFILTRO; k++) {
                    for (int w = 0; w < DIMFILTRO; w++) {
                        soma = soma + (aumentada[i + k][j + w]);
                    }
                }
                media = soma / TAMANHO;
                for (int m = 0; m < DIMFILTRO; m++) {
                    for (int n = 0; n < DIMFILTRO; n++) {
                        somaDesv = somaDesv + ((int) (Math.pow(aumentada[i + m][j + n] - media, 2)));
                    }
                }
                filtrada[i][j] = somaDesv / TAMANHO;

            }
        }
        System.out.println("Imagem filtrada: ");
        mostrarImagem(filtrada, dim);
        return filtrada;
    }

    public static int[][] rodarMatriz90NaoHorario(int[][] imagem, int dim) {
        int[][] rodada90NaoHorario = new int[dim][dim];
        for (int j = 0; j < dim; j++) {
            for (int i = 0; i < dim; i++) {
                rodada90NaoHorario[j][i] = imagem[i][dim - j - 1];
            }
            System.out.println(Arrays.toString(rodada90NaoHorario[j]));
        }
        System.out.println("Imagem rodada sentido anti-horario");
        return rodada90NaoHorario;
    }

    public static void integracao(int[][] imagem, int dim) {
        int[] vetorRegraTrapezio = new int[dim];
        int h = dim - 1;
        for (int i = 0; i < dim; i++) {
            vetorRegraTrapezio[i] = (imagem[i][0] + imagem[i][dim - 1]) * h / 2;
            System.out.println(vetorRegraTrapezio[i]);
        }
    }

    public static void decompor(int[][] imagem, int dim) throws FileNotFoundException {
        double[][] imagemDouble = new double[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                imagemDouble[i][j] = imagem[i][j];
            }
        }
        Matrix imagemMatrix = new Basic2DMatrix(imagemDouble);
        SingularValueDecompositor imagemMatrix2 = new SingularValueDecompositor(imagemMatrix);
        Matrix[] matrizSVD = imagemMatrix2.decompose();
        double[][] matrizU = matrizSVD[0].toDenseMatrix().toArray();
        double[][] matrizD = matrizSVD[1].toDenseMatrix().toArray();
        double[][] matrizV = matrizSVD[2].toDenseMatrix().toArray();
        for (int i = 0; i < matrizU.length; i++) {
            System.out.println(Arrays.toString(matrizD[i]));
        }
        guardarImagemComprimida(matrizU, matrizD, matrizV, dim);
        String nomeFicheiro="ImagemComprimida.txt";
        System.out.println("Imagem Reconstruida:");
        int imagemReconstruida[][]=reconstruirImagem(nomeFicheiro);
        calcularErro(imagemReconstruida, imagem, dim);
    }

    public static void guardarImagemComprimida(double[][] matrizU, double[][] matrizD, double[][] matrizV, int dim) throws FileNotFoundException {
         System.out.println("Deseja guardar o ficheiro? (sim ou nao)");
        String opcao = ler.next();
        while (!opcao.equals("sim") && !opcao.equals("nao")) {
            System.out.println("Opcao invalida introduza de novo");
            System.out.println("Deseja guardar o ficheiro? (sim ou nao)");
            opcao = ler.next();
        }
        if (opcao.equals("sim")) {
        ler.nextLine();
        Formatter imagemComprimida = new Formatter(new File("ImagemComprimida.txt"));
        Formatter output = new Formatter(new File("Imagens/ImagemComprimida.txt"));
        System.out.println("Introduza o nome da imagem");
        String nomeImagem = ler.nextLine();
        imagemComprimida.format("%s%n", "#" + nomeImagem + "  " + dim);
        imagemComprimida.format("%s%n", "");
        int rank = calcularRank(matrizD, dim);
        System.out.println("Introduza o numero de valores unitarios com que pretende comprimir a imagem (ate " + rank + ")");
        int numeroValores = ler.nextInt();
        for (int a = 0; a < numeroValores; a++) {
            imagemComprimida.format("%s%n", matrizD[a][a]);
            output.format("%s%n", matrizD[a][a]);
            imagemComprimida.format(Double.toString(matrizU[a][0]));
            output.format(Double.toString(matrizU[a][0]));
            for (int i = 1; i < matrizU[0].length; i++) {
                imagemComprimida.format(", " + matrizU[a][i]);
                output.format(", " + matrizU[a][i]);
            }
            imagemComprimida.format("%s%n", "");
            output.format("%s%n", "");
            imagemComprimida.format(Double.toString(matrizV[a][0]));
            output.format(Double.toString(matrizV[a][0]));
            for (int i = 1; i < matrizV[0].length; i++) {
                imagemComprimida.format(", " + matrizV[a][i]);
                output.format(", " + matrizV[a][i]);
            }
            imagemComprimida.format("%s%n", "");
            output.format("%s%n", "");
        }
        imagemComprimida.close();
        output.close();
    }
    }

    public static int calcularRank(double[][] matrizD, int dim) {
        int rank = 0;
        for (int i = 0; i < matrizD.length; i++) {
            if (matrizD[i][i] != 0) {
                rank++;
            } else {
                break;
            }
        }
        return rank;
    }

    public static int[][] reconstruirImagem(String nomeFicheiro) throws FileNotFoundException {
        int contadorValores = 0;
        Scanner comprimida = new Scanner(new File(nomeFicheiro));
        String linha1 = comprimida.nextLine();
        String[] linha1Array = linha1.split("  ");
        int dim = Integer.parseInt(linha1Array[1]);
        double[][] matrizU=new double[dim][dim];
        double[][] matrizD=new double[dim][dim];
        double[][] matrizV=new double[dim][dim];
        comprimida.nextLine();
        while (comprimida.hasNextLine()) {
            String valorUnitario=comprimida.nextLine();
            matrizU[contadorValores][contadorValores] = Double.parseDouble(valorUnitario);
            String[] linhaD = comprimida.nextLine().split(",");
            for (int i = 0; i < dim; i++) {
                matrizD[contadorValores][i] = Double.parseDouble(linhaD[i]);
            }
            String[] linhaV = comprimida.nextLine().split(",");
            for (int i = 0; i < dim; i++) {
                matrizV[contadorValores][i] = Double.parseDouble(linhaV[i]);
            }
            contadorValores++;
        }
        contadorValores = 0;
        int rank = calcularRank(matrizU, dim);
        int[][] reconstruida = new int[dim][dim];
        int[][] reconstruidaTemporaria = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                reconstruida[i][j] = 0;
                reconstruidaTemporaria[i][j] = 0;
            }
        }
        do {
            for (int a = 0; a < matrizU.length; a++) {
                for (int b = 0; b < matrizU.length; b++) {
                    reconstruidaTemporaria[a][b] =(int) (matrizU[contadorValores][contadorValores] * matrizD[a][contadorValores] * matrizV[b][contadorValores]);
                }
            }
            contadorValores++;
            for (int i = 0; i < dim; i++) {
                for (int j = 0; j < dim; j++) {
                    reconstruida[i][j] = reconstruida[i][j] + reconstruidaTemporaria[i][j];
                }
            }
        } while (contadorValores < rank);
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                System.out.print(reconstruida[i][j]);
            }
            System.out.println("");
        }
        return reconstruida;
    }
    
    public static void calcularErro(int[][] imagemReconstruida, int[][] imagem, int dim){
        int diferencaTotal=0;
        double erro;
        for(int i=0; i<dim; i++){
            for(int j=0; j<dim; j++){
                diferencaTotal=Math.abs(diferencaTotal+(imagem[i][j]-imagemReconstruida[i][j]));
            }
        }
        erro=diferencaTotal/(dim*dim);
        System.out.println("O erro �: "+erro);
    }
    
    public static int[][] filtroConvolucao(int dim, int[][] imagem, int[][] aumentada) throws FileNotFoundException {
        int[][] mascara = preencherMascara();
        int somaMascara = 0, t = 0, p = 0;
        for (int z = 0; z < mascara.length; z++) {
            for (int y = 0; y < mascara.length; y++) {
                somaMascara = somaMascara + mascara[z][y];
            }
        }
        if (somaMascara != 0) {
            int[][] filtrada = new int[dim][dim];
            int soma;
            for (int i = 1; i < dim + 1; i++) {
                for (int j = 1; j < dim + 1; j++) {
                    soma = 0;
                    for (int x = -1; x < 2; x++) {
                        for (int k = -1; k < 2; k++) {
                            soma = (soma + (aumentada[i + x][j + k]) * (mascara[x + 1][k + 1]));
                        }
                    }
                    soma = soma / somaMascara;
                    if (soma < 0) {
                        filtrada[i - 1][j - 1] = 0;
                    }
                    if (soma > 255) {
                        filtrada[i - 1][j - 1] = 255;
                    } else {
                        filtrada[i - 1][j - 1] = soma;
                    }
                }
            }
            System.out.println("\nImagem Filtrada:\n");
            mostrarImagem(filtrada, dim);
            return filtrada;
        } else {
            System.out.println("ERRO!!\nA soma da matriz mascara � igual a zero!");
            System.out.println("A matriz nao foi alterada\n");
            return imagem;
        }
    }

    public static int[][] preencherMascara() {
        int[][] mascara = new int[3][3];
        int dim = 3;
        for (int i = 0; i < mascara.length; i++) {
            for (int j = 0; j < mascara.length; j++) {
                System.out.println("Introduza o valor da linha " + (i + 1) + " coluna " + (j + 1));
                mascara[i][j] = ler.nextInt();
            }
        }
        System.out.println("\nMascara:\n");
        mostrarImagem(mascara, dim);
        return mascara;
    }
}


